import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import "./style.css"
import { ClearPokemon } from '../../store/actions/pokemonActions';
import { RootState } from '../../types/state.type';
import { Stat } from '../../types/stat.type';
import { PokemonAbility } from '../../types/pokemonAbility.type';
import { PokemonType } from '../../types/pokemonType.type';

const PokemonCard = () => {
    const dispatch = useDispatch();
    const { index, item } = useSelector((state: RootState) => state.pokemon);

    const ref = useRef<HTMLDivElement>(null)

    useEffect(() => {
        const handleClickOutside = (event: MouseEvent) => {
            if (ref.current && !ref.current.contains(event.target as Node)) {
                dispatch(ClearPokemon(index));
            }
        };

        document.addEventListener("mousedown", handleClickOutside);

        return () => {
            document.removeEventListener("mousedown", handleClickOutside);
        };

    }, [dispatch, index, ref]);



    return (

        <div className='PokemonCard'>
            {item && index &&

                <div className='CardContainer' ref={ref}>
                    <img className='closeBtn' onClick={() => dispatch(ClearPokemon(index))} src='/assets/close.svg' alt="closeBtn" />
                    <div className='cardContainerTop'>
                        <img className='img' src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index}.png`} alt={'img' + index} />
                        <div className='dataContainer'>
                            <div>
                                <div className='data'>
                                    <div>name</div>
                                    <div> {item.name} </div>
                                </div>
                                <div className='data'>
                                    <div>type</div>
                                    <div className='pokemonTypes'>
                                        {item.types.map((type: PokemonType) => (
                                            <div key={type.type.name}> {type.type.name}   </div>
                                        ))}
                                    </div>
                                </div>
                                <div className='data'>
                                    <div>base experience</div>
                                    <div> {item.base_experience} </div>
                                </div>
                                <div className='data'>
                                    <div>height</div>
                                    <div> {item.height} </div>
                                </div>
                                <div className='data'>
                                    <div>weight</div>
                                    <div> {item.weight} </div>
                                </div>
                                <div className='data'>
                                    <div>abilities</div>
                                    <div> {item.abilities.map((ability: PokemonAbility) => (
                                        <div key={ability.ability.name} > {ability.ability.name}
                                            <div className='mutedText'>
                                                {ability.is_hidden ? "(hidden ability)" : ""}
                                            </div>
                                        </div>

                                    ))} </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div>
                        <h2 className='label1'>Base Stats</h2>
                        {
                            item.stats.map((state: Stat, index: number) => (
                                <div key={index} className='statsContainer'>
                                    <div className='statsName'> {state.stat.name} </div>
                                    <div> {state.base_stat} </div>
                                    <div className='statsStickContainer'>
                                        <div className='statsStick' style={{ width: `calc(${state.base_stat}%)` }} ></div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>

                </div>
            }
        </div>





    );

};

export default PokemonCard;
