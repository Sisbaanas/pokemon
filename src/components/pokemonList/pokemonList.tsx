import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import "./style.css"
import InfiniteScroll from 'react-infinite-scroll-component';
import PokemonCard from '../pokemonCard/pokemonCard';
import { fetchPokemonRequest } from '../../store/actions/pokemonActions';
import { fetchPokemonListRequest } from '../../store/actions/pokemonListActions';
import Loader from '../loader/loader';
import { RootState } from '../../types/state.type';
import { PokemonListItem } from '../../types/pokemonListItem.type';

const PokemonList = () => {
  const dispatch = useDispatch();
  const { content } = useSelector((state: RootState) => state.pokemonList);
  const { item, loading } = useSelector((state: RootState) => state.pokemon);

  useEffect(() => {
    dispatch(fetchPokemonListRequest());
  }, [dispatch]);

  return (

    <div className='pokemonListContainer'>
      <h1>Pokémon List</h1>
      <div className='pokemonList'>
        <InfiniteScroll

          dataLength={content.length}
          next={() => dispatch(fetchPokemonListRequest())}
          hasMore={true}
          loader={<h4>Loading...</h4>}
          scrollThreshold={0.9}
          scrollableTarget="scrollableDiv"
          className='infiniteScroll'
        >
          {content.map((pokemon: PokemonListItem, index: number) => (
            <div key={index} className='pokemonCard'
              onClick={() => {
                dispatch(fetchPokemonRequest(index + 1));
              }}>
              <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`} alt={pokemon.name} />
              <div className='pokemonName'>{pokemon.name}</div>
            </div>
          ))}
        </InfiniteScroll>
      </div>

      {item && <PokemonCard />}
      {loading && <Loader />}

    </div>


  );

};

export default PokemonList;
