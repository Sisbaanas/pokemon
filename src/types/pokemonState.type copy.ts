import { Pokemon } from "./pokemon.type";

export interface PokemonState {
    item?: Pokemon;
    loading: boolean;
    index?: number;
}
