import { StatType } from "./statType.type";

export interface Stat {
    base_stat: number;
    stat: StatType;

}
