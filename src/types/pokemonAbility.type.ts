import { Ability } from "./ability.type";

export interface PokemonAbility {
    ability: Ability;
    is_hidden: boolean;


}
