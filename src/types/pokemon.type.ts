import { PokemonAbility } from "./pokemonAbility.type";
import { PokemonType } from "./pokemonType.type";
import { Stat } from "./stat.type";

export interface Pokemon {
    stats: Stat[];
    abilities: PokemonAbility[];
    types: PokemonType[];
    height: number;
    weight: number;
    base_experience: number;
    name: string;


}
