import { PokemonListItem } from "./pokemonListItem.type";

export interface PokemonListState {
    content: PokemonListItem[];
    loading: boolean;
    error: null | Error;
    offset: number;
}
