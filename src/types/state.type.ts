import { PokemonListState } from "./pokemonListState.type";
import { PokemonState } from "./pokemonState.type copy";

export interface RootState {
    pokemonList: PokemonListState;
    pokemon: PokemonState;
  }