import { Provider } from 'react-redux';
import './App.css';
import PokemonList from './components/pokemonList/pokemonList';
import store from './store/store';

function App() {

  
  return (
    <Provider store={store}>
      <div className="App">
        <PokemonList />
      </div>
    </Provider>
  );
}

export default App;
