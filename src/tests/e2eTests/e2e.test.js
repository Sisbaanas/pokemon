// e2e.test.js

const puppeteer = require('puppeteer');

describe('End-to-End Test: Pokemon App', () => {
  let browser;
  let page;

  beforeAll(async () => {
    browser = await puppeteer.launch({ headless: false }); // , args: ['--no-sandbox']
    page = await browser.newPage();
    await page.goto('http://localhost:3000');
  });

  afterAll(async () => {
    await browser.close();
  });

  test('Selecting a Pokemon displays modal with details', async () => {

    // Wait for the Pokemon list to load
    await page.waitForSelector('.pokemonCard');

    // Get the content of all Pokemon cards
    const pokemonCardsContent = await page.$$eval('.pokemonCard', (cards) =>
      cards.map((card) => ({
        name: card.querySelector('.pokemonName').textContent,
        imageUrl: card.querySelector('img').src,
      }))
    );

    // Check if each card has content
    expect(pokemonCardsContent.every((card) => card.name && card.imageUrl)).toBe(true);

    const firstPokemonCard = await page.$('.infiniteScroll:first-child .pokemonCard');

    // Click on the first Pokemon card
    await firstPokemonCard.click();

    // Wait for the popup to appear
    await page.waitForSelector('.CardContainer');

    // Check if the popup has the expected content
    const popupContent = await page.$eval('.CardContainer', (popup) => popup.textContent);
    expect(popupContent).toContain('Base Stats');

    // Close the popup
    await page.click('.closeBtn');
    // Wait for the popup to close
    await page.waitForSelector('.CardContainer', { hidden: true });
  }, 20000);

});

