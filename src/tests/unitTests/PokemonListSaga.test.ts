import { runSaga } from "redux-saga";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { fetchPokemonList } from "../../store/Saga/pokemonListSaga";
const mock = new MockAdapter(axios);

beforeEach(() => {
  mock.reset();
});

test('test 1: fetchPokemonList saga handles a successful API call by dispatching the correct success action', async () => {
  const mockResponse = {
    results: ['mocked data']
  };

  // Mock the Axios request
  mock.onGet('https://pokeapi.co/api/v2/pokemon?offset=0').reply(200, mockResponse);

  const dispatched: any[] = [];

  // Run the saga
  await runSaga(
    {
      dispatch: (action: any) => dispatched.push(action),
    },
    () => fetchPokemonList(true),
  ).toPromise();

  // Expectations
  const expectedPayload = {
    content: ['mocked data'],
    offset: 20,
  };

  expect(mock.history.get.length).toBe(1);

  expect(dispatched).toEqual([
    { type: 'FETCH_POKEMON_List_SUCCESS', payload: expectedPayload },
  ]);
});

test('test 2: fetchPokemonList saga handles a failed API call by dispatching the correct failure action', async () => {
  // Mock the Axios request for a failed response
  mock.onGet('https://pokeapi.co/api/v2/pokemon?offset=0').reply(500, { error: 'Internal Server Error' });

  const dispatched: any[] = [];

  // Run the saga
  await runSaga(
    {
      dispatch: (action: any) => dispatched.push(action),
    },
    () => fetchPokemonList(true),
  ).toPromise();

  expect(mock.history.get.length).toBe(1);

  expect(dispatched).toEqual([
    { type: 'FETCH_POKEMON_List_FAILURE', payload: 'Request failed with status code 500' },
  ]);
});

