
import {
  FETCH_POKEMON_REQUEST,
  FETCH_POKEMON_SUCCESS,
  FETCH_POKEMON_FAILURE,
  CLEAR_POKEMON
} from '../actions/pokemonActions';
import { PokemonState } from '../../types/pokemonState.type copy';

const initialState: PokemonState = {
  item: undefined,
  loading: false,
  index: undefined,
};

const pokemonReducer = (state = initialState, action: { type: string; payload: any; }) => {
  console.log(action);
  
  switch (action.type) {
    case FETCH_POKEMON_REQUEST:
      return { ...state, loading: true };
    case FETCH_POKEMON_SUCCESS:
      const newState = { ...state, loading: false, item: action.payload.pokemon, index: action.payload.index };
      document.body.style.overflow = 'hidden';
      return newState
    case FETCH_POKEMON_FAILURE:
      return { ...state };
    case CLEAR_POKEMON:
      document.body.style.overflow = 'visible';
      return { ...state, loading: false, item: undefined };
    default:
      return state;
  }
};

export default pokemonReducer;