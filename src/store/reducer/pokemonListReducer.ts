import { PokemonListState } from '../../types/pokemonListState.type';
import {
  FETCH_POKEMON_List_REQUEST,
  FETCH_POKEMON_List_SUCCESS,
  FETCH_POKEMON_List_FAILURE,
} from '../actions/pokemonListActions';

const initialState: PokemonListState = {
  content: [],
  loading: false,
  error: null,
  offset: 0,
};

const pokemonReducer = (state = initialState, action: { type: string; payload: any; }) => {

  switch (action.type) {
    case FETCH_POKEMON_List_REQUEST:
      return { ...state, loading: true, error: null };
    case FETCH_POKEMON_List_SUCCESS:
      const newState = { ...state, loading: false, content: [...state.content, ...action.payload.content], offset: action.payload.offset };
      return newState
    case FETCH_POKEMON_List_FAILURE:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default pokemonReducer;