import { put, takeLatest, call, delay } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { FETCH_POKEMON_REQUEST, fetchPokemonFailure, fetchPokemonSuccess } from '../actions/pokemonActions';
import axios from 'axios';
import { API_BASE_URL } from '../../config';

export function* fetchPokemon(action: any): SagaIterator {
  try {

    // just to display the loader
    //yield delay(2000);
    const response = yield call(axios.get, API_BASE_URL+`pokemon/${action.payload.index}`);
    yield put(fetchPokemonSuccess(response.data, action.payload.index));
  } catch (error: any) {
    yield put(fetchPokemonFailure(error.message));
  }
}

export function* pokemonSaga(): SagaIterator {
  yield takeLatest(FETCH_POKEMON_REQUEST, fetchPokemon);
}


export default pokemonSaga;
