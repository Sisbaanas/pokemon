import { put, takeLatest, call, select } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { fetchPokemonListRequest, fetchPokemonListSuccess, fetchPokemonListFailure } from '../actions/pokemonListActions';
import axios from 'axios';
import { API_BASE_URL } from '../../config';


export function* fetchPokemonList(isTesting?: boolean): SagaIterator {
  try {
    let offset = 0;
    if (!isTesting) {
      offset = yield select((state) => state.pokemonList.offset);
    }
    
    const response = yield call(axios.get, API_BASE_URL+'pokemon?offset=' + offset);
    
    yield put(fetchPokemonListSuccess(response.data.results, offset + 20));
  } catch (error: any) {
    yield put(fetchPokemonListFailure(error.message));
  }
}

export function* pokemonListSaga(): SagaIterator {
  yield takeLatest(fetchPokemonListRequest().type, () => fetchPokemonList());
}

export default pokemonListSaga;
