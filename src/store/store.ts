import createSagaMiddleware from "@redux-saga/core";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import pokemonListReducer from './reducer/pokemonListReducer'; // Assuming your reducer file is in the same directory
import pokemonListSaga from "./Saga/pokemonListSaga";
import pokemonReducer from "./reducer/pokemonReducer";
import pokemonSaga from "./Saga/pokemonSaga";

const sagaMiddleware = createSagaMiddleware();

const rootReducer = combineReducers({
  pokemonList: pokemonListReducer,
  pokemon: pokemonReducer,
});

const store = configureStore({
  reducer: rootReducer
  ,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(sagaMiddleware),
});

sagaMiddleware.run(pokemonListSaga) 
sagaMiddleware.run(pokemonSaga) 

export default store;