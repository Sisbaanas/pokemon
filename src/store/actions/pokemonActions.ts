import { Pokemon } from "../../types/pokemon.type";

export const FETCH_POKEMON_REQUEST = 'FETCH_POKEMON_REQUEST';
export const FETCH_POKEMON_SUCCESS = 'FETCH_POKEMON_SUCCESS';
export const FETCH_POKEMON_FAILURE = 'FETCH_POKEMON_FAILURE';
export const CLEAR_POKEMON = 'CLEAR_POKEMON';

export const fetchPokemonRequest = (index:number) => ({
  type: FETCH_POKEMON_REQUEST,
  payload: {index}  
});

export const fetchPokemonSuccess = (pokemon: Pokemon,index:number) => ({
  type: FETCH_POKEMON_SUCCESS,
  payload: { pokemon , index },
});

export const fetchPokemonFailure = (error: any) => ({
  type: FETCH_POKEMON_FAILURE,
  payload: error,
});

export const ClearPokemon = (error: any) => ({
  type: CLEAR_POKEMON,
});