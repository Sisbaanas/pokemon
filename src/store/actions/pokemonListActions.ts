import { PokemonListItem } from "../../types/pokemonListItem.type";

export const FETCH_POKEMON_List_REQUEST = 'FETCH_POKEMON_List_REQUEST';
export const FETCH_POKEMON_List_SUCCESS = 'FETCH_POKEMON_List_SUCCESS';
export const FETCH_POKEMON_List_FAILURE = 'FETCH_POKEMON_List_FAILURE';

export const fetchPokemonListRequest = () => ({
  type: FETCH_POKEMON_List_REQUEST,
});

export const fetchPokemonListSuccess = (content: PokemonListItem[], offset: number) => ({
  type: FETCH_POKEMON_List_SUCCESS,
  payload: { content, offset },
});

export const fetchPokemonListFailure = (error: any) => ({
  type: FETCH_POKEMON_List_FAILURE,
  payload: error,
});
